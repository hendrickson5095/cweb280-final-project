<?php
session_start();
$resetID = "";
$db = new DbObject();

function emailLink($email)
{
    global $resetID;
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= "From: TheMostSuperAwesomeWebsiteEver@cst.siast.sk.ca";
    $message = "http://kelcstu05/~cst218/CWEB280FinalProject/reset.php?resetID={$resetID}";
    return mail($email, "Reset Email", $message, $headers);
}

function generateRandomString($length)
{
    $chars = array_merge(range('a', 'z'), range(0, 9));

    return array_reduce(array_rand($chars, $length), function($str, $key) use ($chars)
            {
                return $str.= $chars[$key];
            }, '');
}

function changeResetID($id)
{
    global $resetID;
}

if (isset($_POST["send"]))
{

    $qryResults = $db->select("userID", "cst214Users", "email ='{$_POST['email']}'");
    $numRows = $qryResults->num_rows;
    if ($numRows == 1)
    {
        $row = $qryResults->fetch_row();
        $userID = $row[0];
        $_SESSION["userID"] = $userID;
        $resetID = generateRandomString(10);
        $stmt = $db->prepare("UPDATE cst214Users SET resetID = ? 
            WHERE email = ?");
        $stmt->bind_param("ss", $resetID, $_POST["email"]);
        $stmt->execute();
        emailLink("cst218@cst.siast.sk.ca");
        echo "Email Sent";
    }
    else
    {
        echo 'Invalid email';
    }
}
else if (isset($_POST["reset"]))
{
    if ($_POST["newPassword"] == $_POST["rePassword"])
    {
        $passwordCheck = new PasswordChecker();
        $passwordCheck->changePassword($_POST["newPassword"]);
        $stmt = $db->prepare("UPDATE cst214Users SET resetID = NULL 
            WHERE userID = ?");
        $stmt->bind_param("i", $_SESSION["userID"]);
        $stmt->execute();
        header("Location: http://kelcstu05/~cst218/CWEB280FinalProject/login.php");
    }
    else
    {
        echo 'Passwords do not match';
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./CSS/Style.css">
        <title>Reset Password</title>
    </head>
    <body>
        <div id="framecontentLeft">
            <div class="innertube">
            </div>
        </div>
        <div id="framecontentRight">
            <div class="innertube">
            </div>
        </div>
        <div id="maincontent">
            <div class="innertube">
                <?php
                $form = new GenerateForm();
                if (isset($_GET["resetID"]))
                {
                    if (!isset($_SERVER["HTTPS"]) || !$_SERVER["HTTPS"])
                    {
                        // just checked to see if the current session isusing the https protocol /ssl
                        //now we can use php yo redirect the browser to use https
                        header("HTP/1.1 301 Moved Permanently"); // tells the browser the page is not longer here
                        //tells the browser the new location but really it is just putting https in front 
                        //of the current url
                        header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
                        exit(); //exit the excution of any other php code
                    }

                    $form->startForm("resetPassword", "Reset Password");
                    $form->password("newPassword", "New Password");
                    $form->password("rePassword", "Re-enter New Password");
                    $form->endForm("reset", "Reset Password");
                }
                else
                {
                    $form->startForm("email", "Email Reset Link");
                    $form->textbox("email", "Email");
                    $form->endForm("send", "Send Reset Link");
                }
                ?>
            </div>
        </div>
    </body>
</html>
<?php

function __autoload($className)
{
    $fileName = "$className.class.php";
    if (file_exists("$fileName"))
    {
        //require_once("../classes/$fileName");
    }
    else
    {
        require_once("./classes/$fileName");
    }
}
?>
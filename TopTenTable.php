<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js" ></script>
        <title>Top Ten Images</title>
        <script type="text/javascript" >
        function init() {
                $.get('getTopTen.php', loadContent);

            }
            
       function loadContent(xml) {
        $("#table").css("visibility", "visible");
        var tbl = $('#content').empty(); //find the tbody and empty out the children

        $(xml).find('image').each(
                function() {
                    var inArray = false;
                    var image = $(this); //convert item node to jquery object
                    var row = $('<tr/>');
                    //add table cells with the values from the xml file
                    row.append('<td id="link">' + image.find('link').text());
                    row.append('<td id="imageID">' + image.find('imageID').text());
                    row.append('<td id="metatype" style="display:none">' + image.find('metatype').text());
                    row.append('<td id="caption">' + image.find('caption').text());
                    row.appendTo(tbl);
                });
            }
            
        $(init);
        </script>
    </head>
    <body>
               <table>
            <thead>
                <tr>
                    <th id ="link">Thumbnail</th>
                    <th id=" imageID">ImageID</th>
                    <th id="metatype" style="display:none">Metatype</th>
                    <th id="caption">Caption</th>
                </tr>
            </thead>
            <tbody id="content"></tbody>
        </table>
    </body>
</html>

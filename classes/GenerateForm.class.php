<?php
/**
 * Description of GenerateForm
 *  this class will define HELPER methods to generate HTML forms on webpage
 * we will continue using this class going forward to speed up coding of examples and assignments
 * @author ins226
 */
class GenerateForm {
    // these constants are format strings for use in sprintf calls
    const FORMSTART = '<form id="%s" action="%s" method="%s" %s><fieldset %s><legend>%s</legend>';
    const FORMEND = '<div %s><input type="submit" value="%s" name="%s" /><input type="reset" value="%s" /></div></fieldset></form>';
    //using heredoc notation to define a nicely indented html code output
    const INPUTWRAP = <<<EOT

<div>
    <label for="%s">%s</label>
    %s
</div>
EOT;
// the above line indicates the end of string - nothing else can be on that line
    
    // define a public variable as a flag as to whether or not the getValue method
    // sets the values of the form fields to their corresponding posted values
    // in effect "remembering" what the user has input in the form fields
    public $rememberUserInput = true;// used in getValue method
    

    /**
     * 
     * @param bool $rememberUserInput - used to deterimine whether the values should be remembered on post back (default: true)
     */
    function __construct($rememberUserInput=true) {
        $this->rememberUserInput = $rememberUserInput;
    }

    /**
     * startForm - generates the begining of the form using the passed in params
     * @param string $name - the id of the fomr element
     * @param string $display - is the legend of the fieldset
     * @param string $formExtras - any other attributes to the form that may be required ex: enctype="multipart/form-data" (default: empty string)
     * @param string $action - the page to post to (default: #)
     * @param string $method - the way data is sent to the server (default: POST);
     * @param string $fieldsetExtras - any other attributes to the fieldset element that may be required (default: empty string)
     */
    public function startForm($name, $display, $formExtras="", $action="#", $method="POST", $fieldsetExtras="")
    {
        printf(self::FORMSTART,$name,$action,$method,$formExtras,$fieldsetExtras,$display);
    }

    /**
     * endForm - generates the closing form tags and buttons
     * @param string $nameSubmit - the name of the submit button sent to the server
     * @param string $displaySubmit - the value attribute of the submit button - displayed to the user
     * @param string $displayReset - the value attribute of the reset button - displayed to the user  (default: Reset)
     * @param string $divExtras - any other attributes that may be needed for the div button container (default: empty string)
     */
    public function endForm($nameSubmit, $displaySubmit, $displayReset="Reset", $divExtras="class='submit'")
    {
        printf(self::FORMEND, $divExtras, $displaySubmit, $nameSubmit,$displayReset);
    }
    
    /**
     * getValue - will try to get the posted value for an input from the $_POST or the $_GET arrays
     * if the posted value is not set then the functon with return a default value
     * @param type $name - the name of the posted input
     * @param type $defaultValue - optional return value (default : null)
     * @return string
     */
    private function getValue($name, $defaultValue=null)
    {
        // use a ternary statement to return the user input value or the default value
        // in order to get posted data from the GET or POST we can use the $_REQUEST array
        // check to see if this form is supposed to "remember" the input value
        //      if rememberUserInput is false then automatically return the defaultValue
        return $this->rememberUserInput && isset($_REQUEST[$name]) ?  $_REQUEST[$name] : $defaultValue;
    }


    /**
     * textbox - generates a wrapped text input
     * @param string $name - the name send to the server and the id of the element
     * @param string $display - the label of the field
     * @param int $max - maxlength of the  text box (default: 50)
     * @param string $value - the default value populated in the text box (default: empty string)
     * @param string $extras - any other attributes that may be required (default: empty string)
     */
    public function textbox($name, $display, $max=50, $value="", $extras="")
    {
        $value = $this->getValue($name,$value);
        $input = sprintf('<input type="text" name="%s" id="%s" maxlength="%d" value="%s" %s />',
                $name, $name, $max, htmlentities($value), $extras);
        printf(self::INPUTWRAP, $name, $display, $input);
    }
    
    
    /**
     * password - generates a wrapped password input
     * @param string $name - the name send to the server and the id of the element
     * @param string $display - the label of the field
     * @param int $max - maxlength of the  text box
     * @param string $value - the default value populated in the file input (default: empty string)
     * @param string $extras - any other attributes that may be required (default: empty string)
     */
    public function password($name, $display, $max=16, $value="", $extras="")
    {
        $value = $this->getValue($name,$value);
        $input = sprintf('<input type="password" name="%s" id="%s" maxlength="%d" value="%s" %s />',
                $name, $name, $max, htmlentities($value), $extras);
        printf(self::INPUTWRAP, $name, $display, $input);
    }
    
    
    /**
     * hidden - generates a hidden form input
     * @param string $name - the name send to the server and the id of the element
     * @param string $value - the default value populated in the file input (default: empty string)
     * @param string $extras - any other attributes that may be required (default: empty string)
     */
    public function hidden($name, $value="", $extras="")
    {
        $value = $this->getValue($name,$value);
        //hidden inputs are not visible to the user so no need for the Input wrapper
        printf('<input type="hidden" name="%s" id="%s" value="%s" %s />',
                $name, $name,  htmlentities($value), $extras);        
    }    
    
    /**
     * file - generates a wrapped file input
     * @param string $name - the name send to the server and the id of the element
     * @param string $display - the label of the field
     * @param string $value - the default value populated in the file input
     * @param string $extras - any other attributes that may be required (default: empty string)
     */
    public function file($name, $display, $value="", $extras="")
    {
        $value = $this->getValue($name,$value);
        
        $input = sprintf('<input type="file" name="%s" id="%s" value="%s" %s />',
                $name, $name, htmlentities($value), $extras);
        printf(self::INPUTWRAP, $name, $display, $input);
    }     
    
    /**
     * textbox - generates a wrapped text input
     * @param string $name - the name send to the server and the id of the element
     * @param string $display - the label of the field
     * @param string $value - the default value populated in the text box (default: empty string)
     * @param string $extras - any other attributes that may be required (default: empty string)
     */
    public function textarea($name, $display, $value="", $extras="")
    {
        $input = sprintf('<textarea name="%s" id="%s" %s >%s</textarea>',
                $name, $name, $extras, 
                $this->getValue($name,$value));
        printf(self::INPUTWRAP, $name, $display, $input);
    }
    
    /**
     * checkbox - generates a wrapped checkbox input
     * @param string $name - the name send to the server and the id of the element
     * @param string $display - the label of the field
     * @param string $value - the default value of the cehckbox (default: 1)
     * @param string $checked - whether to mark the check box as checked (default: false)
     * @param string $extras - any other attributes that may be required (default: empty string)
     */
    public function checkbox($name, $display, $value="1", $checked=false, $extras="")
    {
       $checked = $this->getValue($name,$checked) ? 'checked="yes"' : '';
       
       $input = sprintf('<input type="checkbox" name="%s" id="%s" value="%s" %s %s />',
            $name, $name,$value, $checked, $extras);
       
        printf(self::INPUTWRAP, $name, $display, $input);
    }
    
    /**
     * option - generates a single option markup for a select box
     * @param string $display - the option displayed text
     * @param string $value - the option value
     * @param bool $selected - boolean specify the option as selected (default: false)
     * @return string
     */
    private function option($display, $value, $selected=false)
    {
       //search through the array for the current value and if found set option as selected
       $selected = $selected ? 'selected="yes"' : '';
       
       return sprintf("<option value=\"%s\" %s >%s</option>\n",
               htmlentities($value), $selected, $display);
       
    }
       
    /**
     * select - generates a form select box
     * @param string $name - the name and id of the select box
     * @param string $display -  the label of the selectbox
     * @param type $options associative array of option values and text
     * @param string $selectedValue - value of the default option to be selected (default: empty string)
     * @param string $extras - any other attributes that maybe needed (default: empty string)
     */
    public function select($name, $display, $options, $selectedValue="", $extras="")
    {
        $postedValues = $this->getValue($name,$selectedValue);
        //save the orignal name in $id to use as the id of the select box
        $id = $name; 
        //if multiple select then append '[]' to the name to force php to parse the multiple selected option into an array
        $name .= preg_match("/multiple/i", $extras) ? '[]' : '';

        // if there are multiple selected options then php will parse the posted select options into an array 
        // so we check to see if postedValues is not an array
       if(!is_array($postedValues)){
           //the selectedValues param my be a comma separated list
           //so we use the explode function split the string on the commas and
           //create an array. Later will search through the array using in_array
           $postedValues =  explode(',', $postedValues);
       }

        $optionsMarkUp = "";        
        foreach($options as $valueOption=>$displayOption)
        {
            //if the current option value exists in the postedValues array mark the current option as selected
            //use the in_array function to search the postedValue array for the current option value
            //in_ array return a boolean value
            $selected = in_array($valueOption, $postedValues);
            
            //append the select option string to the markup
            $optionsMarkUp .= $this->option($displayOption, $valueOption, $selected);
        }
        $input = sprintf('<select name="%s" id="%s" %s >
            %s
            </select>',
            $name, $id, $extras, $optionsMarkUp);
        printf(self::INPUTWRAP, $name, $display, $input);
    }    

    /**
     * multiselect - alias of select method with extras param of 'multiple="multiple" '
     * @param string $name - the name and id of the select box
     * @param string $display -  the label of the selectbox
     * @param type $options associative array of option values and text
     * @param string $selectedValues - comma separated list of value of the default options to be selected (default: empty string)
     * @param string $extras - any other attributes that maybe needed (default: empty string)
     */
    public function multiselect($name, $display, $options, $selectedValues="", $extras="")
    {
        //adding '[]' to the name will indicate to php to save the multiple values in an array
        // specifying 'multiple="multiple" ' in the extras param tells the browser render a select as a listbox
        $this->select($name, $display, $options, $selectedValues, 'multiple="multiple" '.$extras);
    }    
    
    /**
     * groupItem - generates a single form input markup for a input group
     * @param bool $type - the type of form input (default: radio)
     * @param string $name - the name of the input group (also used to generate a unique id)
     * @param string $display - the label of the form input
     * @param string $value - the form input value (also used to generate a unique id)
     * @param bool $checked - whether to mark the current form input as checked
     * @param string $extras - any other attributes that maybe needed (default: empty string)
     * @return string
     */
    private function groupItem($type, $name, $display, $value, $checked, $extras="")
    {
       //determine whether to mark the current form input as checked 
       $checked = $checked ? 'checked="yes"' : '';
       
       //create a unique id from the name and cleaned value of the form input
       //the id is used by label adjacent to the form input
       $id = preg_replace("/\W/i", "" , $name.$value);
       
       return sprintf('<div><input type="%s" name="%s" id="%s" value="%s" %s %s /><label for="%s">%s</label></div>
',$type, $name, $id, htmlentities($value), $checked, $extras, $id, $display);
       
    }    


    /**
     * radioGroup - generates a wrapped group of radio buttons
     * @param type $name - the name of the radio group
     * @param type $display - the label for the radio Group
     * @param type $options - associative array of radio values and labels
     * @param type $selectedValue - the value of the default radio to be checked (default: empty string)
     * @param type $extras - any other attributes that maybe needed (default: empty string)
     */
    public function radioGroup($name, $display, $options, $selectedValue="", $extras="")
    {
        $postedValue = $this->getValue($name,$selectedValue);
        
        // surround the radio groups with a span tag
        $radioMarkUp = "<span>";
        
        //loop through the options array and call the option method
        foreach($options as $valueRadio=>$displayRadio)
        {
            //append the radiobutton string to the markup
            $radioMarkUp .= $this->groupItem("radio", $name,
                    $displayRadio, $valueRadio, 
                    $postedValue==$valueRadio, $extras);
        }
        
        $radioMarkUp .= "</span>";
        
        // no need to specify the name this time since each radio button will have its own label
        printf(self::INPUTWRAP, "", $display, $radioMarkUp);

    }

    /**
     * checkboxGroup - generates a wrapped group of checkboxes
     * @param string $name - the name of the checkbox group
     * @param string $display - the label for the checkbox Group
     * @param string $options - associative array of checkbox values and labels
     * @param string $selectedValue - comma separated list of values of the default inputs to be check (default: empty string)
     * @param string $extras - any other attributes that maybe needed (default: empty string)
     */
    public function checkboxGroup($name, $display, $options, $selectedValue="", $extras="")
    {
        // php is parsing the posted checkbox values as an array so we expect postedValues to be an array
        $postedValues = $this->getValue($name,explode(',',$selectedValue));
        
        // surround the radio groups with a span tag
        $checkboxMarkUp = "<span>";
        
        //adding '[]' to the name will indicate to php to save the multiple posted values in an array
        $name .='[]';
        
        //loop through the options array and call the option method
        foreach($options as $valueCheckbox=>$displayCheckbox)
        {
            //if the current checkbox value exists in the postedValues array, mark the current checkbox as checked
            //use the in_array function to search the postedValue array for the current checkbox value
            //in_array returns a boolean value            
            $checked = in_array($valueCheckbox, $postedValues);
            
            //append the chekbox string to the markup
            $checkboxMarkUp .= $this->groupItem("checkbox", $name,
                    $displayCheckbox, $valueCheckbox, $checked, $extras);
        }
        
        $checkboxMarkUp .= "</span>";
        
        // no need to specify the name this time since each check box will have its own label
        printf(self::INPUTWRAP, "", $display, $checkboxMarkUp);

    }
    
    
    
}

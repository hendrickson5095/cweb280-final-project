<?php

require_once 'DbObject.class.php';
/**
 * Description of XmlDbObject
 *
 * @author cst239
 */
class XmlDbObject extends DbObject
{
    private $XSLT;
    
    public function __construct($XSLT="", $server = "kelcstu05.cst.siast.sk.ca", $user = "CST214", $password="XFAVXK", $schema="CST214") 
    {
        $this->XSLT = $XSLT;
        parent::__construct($server, $user, $password, $schema);
    }
    
    function selectToXml($rootName, $columnList, $tableList, $condition = "", $sort = "", $other = "")
    {
        $queryResult = $this->select($columnList, $tableList, $condition, $sort, $other);
        //we want to the table name as the child node name
        //but we need to clean up the name first
        $childName = explode(" ", $tableList);
        $childName = strtolower($childName[0]);
        $childName = rtrim($childName, "s");
        
        //call help function to loop through and read into 2D array
        return $this->getXml($queryResult, $rootName, $childName);
    }
    
    function getXml($queryResult, $rootName, $childName="row")
    {
        //init 2D array
        $infoData = array();
        
        while($rowData = $queryResult->fetch_assoc())
        {
            $infoData[] = $rowData; //add current rowdata to 2d array
        }
        //call the recursive function to add items to simpleXML node
        return $this->convertToXml(null, $infoData, $rootName, $childName);
    }
    
    function convertToXml($obXml, $infoData, $rootName, $childName="row")
    {
        //handle null obXml create new node
        if($obXml == null)
        {
            $obXml = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8' ?><$rootName />");
        }
        
        
        foreach ($infoData as $key=>$value)
        {
            if(is_array($value))
            {
                $currNode = $obXml->addChild($childName);
                $this->convertToXml($currNode, $value, $childName);
            }
            else
            {
                //just add the element to the xmldoc
                $obXml->addChild($key, htmlspecialchars($value));
            }
           
        }
         return $obXml;
    }
}
?>

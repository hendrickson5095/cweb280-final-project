<?php
class PasswordChecker
{
    
    public function isValid($username, $password)
    {
        $result = false;
        $db = new DbObject();
        $qryResults = $db->select("password, userID", "cst214Users", "username ='$username'");
        $numRows = $qryResults->num_rows;
        if ($numRows == 1)
        {
            $row = $qryResults->fetch_row();
            $encriptedPassword = crypt($password, $row[0]);
            $result = $row[0] == $encriptedPassword;
            if ($result)
            {
                $_SESSION["userID"] = $row[1];
            }
        }
        $qryResults->close();
        
         return $result;
    }
    
        /**
     * Purpose: Add a user into the password list
     * @param string $username The username to add
     * @param string $password The password associated with the username
     * @return boolean TRUE if the user was successfully added,
     *   FALSE otherwise
     */
    public function addUser( $username, $password, $email) 
    {
        $db = new DbObject();
        // Create the array to use with the insert method
        $record["username"] = $username;
        $record["password"] = crypt( $password, '$2a$05$cstcweb280cstcweb280ab' );
        $record["email"] = $email;
        
        // Open a database connection
        
        

        // Insert the user into the Password database
        $numRows = $db->insert( $record, "cst214Users" );
        $_SESSION["userID"] = $db->getInsertID();
        return ( $numRows == 1 );
    }
    
    public function changePassword($password)
    {
        $db = new DbObject();
        $stmt = $db->prepare("UPDATE cst214Users SET password = ? 
            WHERE userID = ?");
        $stmt->bind_param("si",crypt( $password, '$2a$05$cstcweb280cstcweb280ab' ), $_SESSION["userID"]);
        $stmt->execute();
        $numRows = $stmt->affected_rows;

        return ( $numRows == 1 );
        
    }

}
<?php

/**
 * The DbObject class represents a connection to a MySQL server.
 * With objects of this class, we'll be able to create and execute
 * query statements.  This is a convenience class, wrapped around
 * the mysqli object.
 */
class DbObject
{

    /**
     * The database connections
     */
    private $dbConnect;

    /**
     * Purpose: To create a connection to a MySQL server and
     *   open a database on that server.
     * 
     * @param string $server - name of the MySQL server
     * @param string $user - name of the MySQL user
     * @param string $password - user's password
     * @param string $schema - name of the schema to use
     */
    public function __construct($server = "kelcstu05.cst.siast.sk.ca", $user = "CST214", $password="XFAVXK", $schema="CST214")
    {

        // Create a mysqli object, and assign it to the internal attribute
        $this->dbConnect = new mysqli($server, $user, $password, $schema);

        // If the connection failed
        // Display an error message
        // Exit        
        if ($this->dbConnect->connect_errno)
        {
            echo "<p>Failed to connect to database: " .
            $this->dbConnect->connect_error . "</p>\n";
            exit();
        }
    }

    /**
     * Purpose: This routine will run the query that is provided by the query
     *   string that is passed in.  If the query fails, exit ungracefully.
     * @param string $qryString The SQL query string that is to be run
     * @return mysqli_result The result of the query
     */
    public function runQuery($qryString)
    {
        // Execute the query
        $qryResult = $this->dbConnect->query($qryString);

        // IF the query failed
        if ($qryResult)
        {
            // Return the result of the query
            return $qryResult;
        }
        else
        {
            // ELSE
            // Print an error message, then exit
            echo "<p>Query $qryString couldn't execute</p>\n";
            exit();
        }
    }

    /**
     * Purpose: Perform a SELECT query on the database
     * @param string $columnList List of columns to be selected
     * @param string $tableList List of tables to select from
     * @param string $condition Optional SQL condition to select with
     * @param string $sort Optional SQL sort clause to apply
     * @param string $other Optional any other SQL clauses to apply
     * @return mysqli_result The result of the SELECT query, or FALSE if
     *   the query fails
     */
    public function select($columnList, $tableList, $condition = "", $sort = "", $other = "")
    {
        // Create the basic SELECT statement
        $qryStmt = "SELECT $columnList FROM $tableList";

        // If a condition is specified, add it to the query
        if ($condition != "")
        {
            $qryStmt .= " WHERE $condition";
        }

        // If a sort order is specified, add it to the query
        if ($sort != "")
        {
            $qryStmt .= " ORDER BY $sort";
        }

        // Add any other SQL clauses if they've been specified
        if ($other != "")
        {
            $qryStmt .= " $other";
        }

        // Execute the query, and return the result
        return $this->runQuery($qryStmt);
    }

    /**
     * Purpose: This method will add a new record to the specified table
     * @param array $newRecord An associative array of the field names
     *   (the array index) and the values to be inserted (the array values)
     * @param string $tableName The name of the table to add the record to
     * @return int The number of rows inserted
     */
    public function insert($newRecord, $tableName)
    {
        // Construct the field name and value lists
        $fieldList = "( ";
        $valueList = "( ";

        foreach ($newRecord as $field => $value)
        {
            $fieldList .= $field . ", ";
            // Don't forget to escape the user-supplied value, in order
            // to prevent an SQL injection attack!!!
            $valueList .= "'" .
                    $this->dbConnect->real_escape_string($value) . "', ";
        }

        // We've finished adding all the field names and values to their
        // respective lists, so delete the final comma and space.
        $fieldList = rtrim($fieldList, ", ");
        $valueList = rtrim($valueList, ", ");

        $fieldList .= " )";
        $valueList .= " )";

        // Perform the insertion
        $insStatement = "INSERT INTO " . $tableName . " " .
                $fieldList . " VALUES " . $valueList . ";";
        echo "<p>Insert statement is: " . $insStatement . "</p>\n";
        // return 1;

        $this->runQuery($insStatement);

        // Return the number of affected rows
        return $this->dbConnect->affected_rows;
    }

    /**
     * Purpose: Display the results of a database query
     * @param mysqli_result $qryResults Results of a previous database query
     */
    static public function displayRecords($qryResults)
    {
        // Display the opening table tag
        echo "<table>\n";

        // Display a table row opening tag
        echo "    <tr>";

        // LOOP for all query result columns
        foreach ($qryResults->fetch_fields() as $fieldInfo)
        {
            // Display the column name within a table header tag
            echo "<th>{$fieldInfo->name}</th>";
        }

        // Display a table row closing tag
        echo "</tr>\n";

        // LOOP for all the query rows returned
        while ($row = $qryResults->fetch_row())
        {
            // Display a table row opening tag
            echo "    <tr>";

            // LOOP for all the query result columns
            for ($i = 0; $i < $qryResults->field_count; $i++)
            {
                // Display the value of this query result row and column
                echo "<td>{$row[$i]}</td>";
            }
            // Display a table row closing tag
            echo "</tr>\n";
        }

        // Display the closing table tag
        echo "</table>\n";
    }

    /**
     * Purpose: Creates an associative array to be used with the GenerateForm
     *   class' methods that populate lists.
     * @param mysqli_result $qryResults The query result record set.  The
     *   result should consist of two columns: the first column will contain
     *   an ID, and the second will contain the corresponding text.
     * @return array An associative array, where the array index comes from
     *   the qryResults' first column, and the array value comes from the
     *   qryResults' second column.
     */
    static public function createArray($qryResults)
    {
        // Create an empty result array
        $result = array();

        // LOOP through all rows in the result set
        while ($row = $qryResults->fetch_row())
        {
            // Set an entry in the result array with the index as the first
            // column in the result set, and the value as the second column
            $result[$row[0]] = $row[1];
        }

        // Return the result array
        return $result;
    }

    public function update($values, $tableName, $primaryKey)
    {
        $sql = "UPDATE $tableName SET ";
        foreach ($values as $fieldName => $fieldValue)
        {
            if ($fieldName != $primaryKey)
            {
                $sql .= "$fieldName = " . $this->dbConnect->real_escape_string($fieldValue) . ", ";
            }
        }
        $sql = rtrim($sql, ", ");
        $sql .= "WHERE " . $primaryKey . "='" . $values[$primaryKey] . "'";

        //Debugging
        //echo "<p>Update statement: $updateStatement</p>";
        $this->runQuery($sql);
    }

    public function prepare($query)
    {
        return $this->dbConnect->prepare($query);
    }

    /**
     * Purpose: Close the database connection
     * The destructor gets called when the object goes out of scope
     * (function terminates, program ends).  Rather than having a separate
     * close method (which might be a good idea anyways, because then we can
     * close early if we want), we'll just close the connection here.
     */
    public function __destruct()
    {
        $this->dbConnect->close();
    }

    public function getInsertID()
    {
        return $this->dbConnect->insert_id;
    }

}

?>

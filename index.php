<?php
session_start();
require_once './classes/DbObject.class.php';
unset($_SESSION['lastAction']);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./CSS/Style.css">
        <title>Homepage</title>
    </head>
    <body>
        <div id="framecontentLeft">
            <div class="innertube">
            </div>
        </div>
        <div id="framecontentRight">
            <div class="innertube">
            </div>
        </div>
        <div id="maincontent">
            <div class="innertube">
                <h1>The Most Super Awesome Website Ever!</h1>

                <div>
                    <ul class="inline">
                        <?php
                        if (isset($_POST["change"]))
                        {

                            $passwordCheck = new PasswordChecker();
                            if ($_POST["newPassword"] == $_POST["rePassword"])
                            {
                                if ($passwordCheck->isValid($_SESSION["username"], $_POST["oldPassword"]))
                                {
                                    $passwordCheck->changePassword($_POST["newPassword"]);
                                    echo 'Success';
                                }
                                else
                                {
                                    echo 'Old Password is invalid';
                                }
                            }
                            else
                            {
                                echo 'Passwords do not match';
                            }
                        }

                        if (isset($_GET["logout"]))
                        {
                            unset($_SESSION["username"]);
                            unset($_SESSION["userID"]);
                            unset($_SESSION["admin"]);
                        }

                        if (!isset($_SESSION["username"]))
                        {
                            //logged out

                            echo '<li><a href="login.php">Log in</a></li>
                                    <li class ="last"><a href="registration.php">Register</a></li>';
                        }
                        else
                        {
                            if (isset($_GET["changePassword"]))
                            {
                                $form = new GenerateForm();
                                $form->startForm("changePassword", "Change Password");
                                $form->password("oldPassword", "Old Password");
                                $form->password("newPassword", "New Password");
                                $form->password("rePassword", "Re-enter New Password");
                                $form->endForm("change", "Change Password");

                                if (isset($_POST["change"]))
                                {
                                    echo "<a href='index.php'>Back</a>";
                                }
                            }
                            else
                            {
                                //Logged in
                                echo '<li><a href="index.php?logout=true">Logout</a></li>
                                      <li><a href="personalPage.php">Personal Page</a></li>';
                                if (isset($_SESSION["admin"]))
                                {
                                    if ($_SESSION['admin'] == 'Y')
                                    {
                                        echo '<li> <a href="index.php?changePassword=true">Change password</a></li><li class="last"> <a href="admin.php">Admin</a></li>';
                                    }
                                }
                                else
                                {
                                    echo '<li class="last"> <a  href="index.php?changePassword=true">Change password</a></li>';
                                }
                            }
                        }

                        if (!isset($_GET["changePassword"]))
                        {
                            ?>
                        </ul>
                    </div>

                    <h3>The Most Super Awesome Images Ever!</h3>
                    <table class="galleryTable">
    <?php
    $db = new DbObject();

    $sql = "SELECT imageID, metatype, caption FROM cst214Images ORDER BY views LIMIT 10";

    $stmt = $db->prepare($sql);

    $stmt->bind_result($imgId, $metatype, $caption);
    $stmt->execute();

    $counter = 1;
    while ($stmt->fetch())
    {
        $link = "<a href='image.php?img=$imgId' ' ><img src='../wide_open/$imgId.$metatype' title='$caption' height='128px' width='128px'></a>";
        if ($counter % 2 == 1)
        {
            echo "<tr><td>$link</td>";
        }
        else
        {
            echo "<td>$link</td></tr>";
        }
        $counter++;
    }
    $stmt->close();
}
?>
                </table>
            </div>
        </div>
    </body>
</html>
<?php

function __autoload($className)
{
    $fileName = "$className.class.php";
    if (file_exists("$fileName"))
    {
        //require_once("../classes/$fileName");
    }
    else
    {
        require_once("./classes/$fileName");
    }
}
?>

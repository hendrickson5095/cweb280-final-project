<?php
session_start();
$ssUserID = &$_SESSION["userID"];
$errorMessage = "";
//logical block of code
$path = "../wide_open/"; //define a global variable for the path to save files to
$file = null; //define another global which we use as a flag of a succesful uploa

function checkFile($file)
{
    $flag;
    $pattern = "#^(image/png)|(image/gif)|(image/jpg)#i";
    if (preg_match($pattern, $file))
    {

        $flag = true;
    }
    else
    {
        echo "($file) is unable to uploaded. <br />";
        $flag = false;
    }
    return $flag;
}

function generateRandomString($length)
{
    $chars = array_merge(range('a', 'z'), range(0, 9));

    return array_reduce(array_rand($chars, $length), function($str, $key) use ($chars)
            {
                return $str.= $chars[$key];
            }, '');
}

function redirect($imageID)
{
    header("Location: image.php?img=$imageID");
}

//Modified From Class Does upload of the image file Called by UploadFile.
function handleUpload($filename)
{
    global $path, $file; //tell this function that the $file and $path variables are global
    $return = "";
    //check to see if file was uploaded properly
    //first use isset to see if the form was posted
    // second use the function is_uploaded_file to ensure we are dealing with an uploaded file *hack prevention*
    if (is_uploaded_file($_FILES["file"]["tmp_name"]))
    //$_FILES is a special 2D associative array for handling posted files
    // the second key is used to refer to various properties of the uploaded file
    // tmp_name = the temporary location where uploaded files get save by the server
    // size = the number of bytes of the uploaded file
    // name = the name of the file specified by the client browser i.e. friendly name
    {
        // check the file size and ensure it is not larger than 250KB
        if ($_FILES["file"]["size"] > 256000)
        {
            $return = "File must be smaller than 250 KB - the current file is (bytes): " . $_FILES["file"]["size"];
        }
        // perform various checks on the file to make sure it meets our specifc limitations
        else // procress the file
        {
            $file = $path . $filename; // the final path and name for the uploaded file
            // use another php function to move the file from the temp location to the wide_open location
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $file))
            {
                $return = $filename . " was successfully uploaded and moved!";
            }
            else
            {
                $return = $_FILES["file"]["name"] . " failed to upload and/or move!";
            }
        }
    }
    return $return;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./CSS/Style.css">
        <title>Personal Page</title>
        <style>
            
            #error
            {
                color: red;
            }
        </style>
    </head>
    <body>
        <div id="framecontentLeft">
            <div class="innertube">
            </div>
        </div>
        <div id="framecontentRight">
            <div class="innertube">
            </div>
        </div>
        <div id="maincontent">
            <div class="innertube">
                <h1>The Most Super Awesome Personal Page Ever!</h1>
                
                <table class="galleryTable">
                    <?php
                    if (!isset($_SESSION["loggedIn"]))
                    {
                        //logged out
                        echo '<div class ="rkight">
                <ul class="inline">
                    <li><a href="login.php">Log in</a></li>
                    <li class ="last"><a href="registration.php">Register</a></li>
                </ul>
            </div>';
                    }
                    else if (isset($_SESSION["userID"]))
                    {
                        $db = new DbObject();
                        echo '<ul class="inline">
                                    <li><a href="index.php?logout=true">Logout</a></li>
                                    <li><a href="index.php">Index</a></li>
                                    <li><a href="personalPage.php">Personal Page</a></li>
                                    <li class="last"><a href="index.php?changePassword=true">Change password</a></li>
                                </ul>';
                        $query = "SELECT username,imageID, metatype, caption FROM cst214Users U JOIN cst214Images I ON  U.userID=I.userID WHERE U.userID = ?";
                        $stmt = $db->prepare($query);
                        $stmt->bind_param("i", $ssUserID);
                        $stmt->bind_result($username, $imgId, $metatype, $caption);
                        $stmt->execute();

                        $counter = 1;
                        while ($stmt->fetch())
                        {
                            $link = "<a href='image.php?img=$imgId' ' ><img src='../wide_open/$imgId.$metatype' title='$caption' height='48px' width='48px'></a>";
                            if ($counter % 2 == 1)
                            {
                                echo "<tr><td>$link</td>";
                            }
                            else
                            {
                                echo "<td>$link</td></tr>";
                            }
                            $counter++;
                        }

                        $stmt->close();
                    }
                    ?>
                </table>
                <?php
                $form = new GenerateForm();
                $form->startForm("Upload", "Upload Image", 'enctype="multipart/form-data"');
                $form->textarea("caption", "Caption");
                $form->file("file", "File to Upload");
                $form->endForm("subUpload", "Upload Image");


                if (isset($_POST["subUpload"]))
                {
                    $cap = $_POST['caption'];
                    $meta = $_FILES['file']["type"];
                    //Setup the id 
                    $imageID = generateRandomString(7);
                    $meta = substr($meta, -3);
                    $filename = "$imageID.$meta";
                    if ($cap != "")
                    {
                        if (checkFile($_FILES['file']["type"]))
                        {
                            if ($_FILES["file"]["size"] < 256000)
                            {
                                //Upload the data to database
                                $db = new DbObject();
        //          $query = "UPDATE cst214Images SET imageID=?,metatype=?,caption=?, userID=? WHERE userID=?";
                                $query = "INSERT INTO cst214Images (imageID, metatype, caption, userID) VALUES(?,?,?,?)";
                                $stmt = $db->prepare($query);
                                $stmt->bind_param("sssi", $imageID, $meta, $cap, $ssUserID);

                                $stmt->execute();
                                $stmt->close();
                                //Upload image to wide open
                            }
                            $upload = handleUpload($filename);
                            echo "<br/>" . $upload;
                            redirect($imageID);
                        }
                    }
                    else
                    {
                        $errorMessage = "Empty caption";
                    }
                }
                ?>
                 <p id="error"><?php echo $errorMessage ?></p>
            </div>
           
        </div>
    </body>
</html>
<?php

function __autoload($className)
{
    $fileName = "$className.class.php";
    if (file_exists("$fileName"))
    {
        //require_once("../classes/$fileName");
    }
    else
    {
        require_once("./classes/$fileName");
    }
}
?>

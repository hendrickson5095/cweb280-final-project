<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
session_start();
require_once './classes/DbObject.class.php';
require_once './classes/GenerateForm.class.php';


$db = new DbObject();
$imgId = $_GET['img'];
$errorMessage = "";
if (!isset($_SESSION['imageViewed']))
{
    $db->runQuery("UPDATE cst214Images SET views = views + 1 where imageID = '$imgId'");
    $_SESSION['imageViewed'] = true;
}

if (isset($_POST['subComment']))
{
    if (true)
    {
        $sessUserID = $_SESSION["userID"];
        if (isset($_POST["CommentArea"]))
        {
            $comment = $_POST["CommentArea"];
            if ($comment != "")
            {
                $date = date_create(null, timezone_open("America/Regina"));
                $formatDate = date_format($date, "Y-m-d");

                $stmt = $db->prepare("INSERT INTO cst214Comments (imageID, comment, postDate, userID) VALUES (?,?,?,?)");
                $stmt->bind_param("sssi", $imgId, $comment, $formatDate, $sessUserID);
                $stmt->execute();
                $stmt->close();
            }
            else 
            {
                $errorMessage = "Empty comment";
            }
        }
    }
}

if (isset($_GET['ImgRate']))
{
    
    $db = new DbObject();

    $rate = $_GET['ImgRate'];

    $query = "UPDATE cst214Images SET rating = rating + ? WHERE imageID = ?";

    $state = $db->prepare($query);
    $state->bind_param('is', $rate, $imgId);
    $state->execute();

    $state->close();
    
    
}

if (isset($_GET["report"]))
{
    $reported = "Y";
    $db = new DbObject();
    $query = "UPDATE cst214Images SET reported = ? WHERE imageID=?";
    $stmt = $db->prepare($query);
    $stmt->bind_param("ss", $reported, $imgId);
    $stmt->execute();
    $stmt->close();
}

if (isset($_GET["remove"]))
{
    $db = new DbObject();
    $selQuery = "Select imageID, metatype FROM cst214Images";
    $stmt4 = $db->prepare( $selQuery );
    $stmt4->bind_result($imageID, $metatype);
    $stmt4->execute();   
    $stmt4->fetch();
    $stmt4->close();
    
    $metatype = substr($metatype, -3);
    $name =$imageID.$metatype;
    echo"I is here  my name is".$name;
    
    $filename = "../wide_open/$name";
    unlink(realpath($filename));
    
    $db2 = new DbObject();
    $query = "DELETE FROM cst214Images WHERE imageID=?";
    $stmt5 = $db2->prepare($query);
    $stmt5->bind_param("s", $imgId);
    $stmt5->execute();
    $stmt5->close();
    header("Location: personalPage.php");
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="./CSS/Style.css">
        <script type="text/javascript" src="//code.jquery.com/jquery-2.0.3.min.js" ></script>
        <script>
            $(document).ajaxError(function(event, req, settings) {
                $("#msg").append("<li> AJAX Error: " + settings.url + "</li>").show();
            });
            $(function() {
                $('#upVote').click(function()
                {
                    var rate = 1;
                    alterRatings(rate);
//                    $(this).attr("src", "./Images/upVoteSel.png");
                });
                $('#downVote').click(function()
                {
                    var rate = -1;
                    alterRatings(rate);
//                    $(this).attr("src", "./Images/downVoteSel.png");
                });
            });

            function alterRatings(rate)
            {
//                alert("it worked: " + rate);
                $.get('image.php', {img: '<?php echo $imgId ?>', ImgRate: rate});
                window.location.replace("image.php?img=<?php echo $imgId ?>");
            }
        </script>
        <title>One of the most awesome images in the world</title>
        <style>
            #error
            {
                color: red;
            }
        </style>
    </head>
    <body>
        <div id="framecontentLeft">
            <div class="innertube">
            </div>
        </div>
        <div id="framecontentRight">
            <div class="innertube">
            </div>
        </div>
        <div id="maincontent">
            <div class="innertube">
                <h1>'One' Of The Most Super Awesome Images Ever!</h1>
<?php
$stmt1 = $db->prepare("SELECT metatype, rating, views, caption, userID, reported FROM cst214Images WHERE imageID = ?");

$stmt1->bind_param("s", $imgId);
$stmt1->bind_result($metatype, $rating, $views, $caption, $imageUser, $reported);
$stmt1->execute();
$stmt1->fetch();

echo "<ul class='inline'>";
if (!isset($_SESSION["username"]))
{
    //logged out

    echo '<li><a href="index.php">Home</a></li>
                          <li><a href="login.php">Log in</a></li>
                          <li class ="last"><a href="registration.php">Register</a></li>';
}
else
{
    //Logged in
    echo '<li><a href="index.php">Home</a></li>
                          <li><a href="index.php?logout=true">Logout</a></li>
                          <li class="last"><a href="personalPage.php">Personal Page</a></li>';
}
echo "</ul>";

echo "<h2>$caption</h2>";

if ($reported == 'Y')
{
    $imgLocation = "./Images/reported.png";
}
else
{
    $imgLocation = "../wide_open/$imgId.$metatype";
}

echo "<div id='ImageContainer'>
                <img id='Image' src=$imgLocation />
                    <div id ='ratings'>
                        <b>Rating:</b> $rating<br/>
                        <b>Views:</b> $views   <br/>
                        <img id='upVote' src='./Images/upVote.png' />
                        <img id='downVote' src='./Images/downVote.png' /><br />";

if ($reported != "A" && $reported != "Y")
{
    echo"<a href='image.php?img=$imgId&report=y'>Report Image</a><br/>";
}

if ($imageUser == $_SESSION['userID'])
{
    echo "<a href='image.php?img=$imgId&remove=y'>Remove Image</a>";
}
echo "</div>
                </div>";
$stmt1->close();
?>

                <script type="text/javascript">
                    var img = document.getElementById("Image");
                    img.onload = function() {
                        var ratingContainer = document.getElementById("ratings");
                        ratingContainer.style.left = this.width + 20 + "px";
                    }
                </script>
<?php
$stmt2 = $db->prepare("SELECT c.commentID, c.comment, c.userID, u.username, c.postDate
                            FROM cst214Comments c
                            JOIN cst214Users u ON u.userID = c.userID
                            WHERE imageID = ?
                            ORDER BY rating");
$stmt2->bind_param("s", $imgId);
$stmt2->bind_result($commentID, $comment, $userId, $username, $postDate);
$stmt2->execute();
?>

                <div id="commentContainer">
                    <h3>Comments</h3>
<?php
while ($stmt2->fetch())
{
    echo "<table id='CommentTable'><tbody>";
    echo "<tr id='row1'><td colspan=2>
            <span id='username' class='left'>$username</span>
            <span id='date' class='right'>$postDate</span></td></tr>";
    echo "<tr id='row2'><td colspan='2' id='comment'>$comment</td></tr>";
    echo "<tr><td><img class='left' src='./Images/upVote.png'/></td>
        <td><img class='right' src='./Images/downVote.png'/></td></tr>";
    echo "</tbody></table>";
}
$stmt2->close();
?>

                    <?php
                    $form = new GenerateForm();
                    $form->startForm("frmAddComment", "", "");
                    ?>
                    <div>
                        <label for="CommentArea" id="CommentAreaLabel">Add Comment</label>
                        <textarea name="CommentArea" id="CommentArea"></textarea>
                    </div>
<?php
$form->endForm("subComment", "Add Comment", "Reset");
?>
                </div>
            </div>
            <p id="error"><?php echo $errorMessage; ?></p>
        </div>
    </body>
</html>

<?php
session_start();
if (isset($_SERVER["HTTPS"]))
{
    // just checked to see if the current session isusing the https protocol /ssl
    //now we can use php yo redirect the browser to use https
    header("HTP/1.1 301 Moved Permanently"); // tells the browser the page is not longer here
    //tells the browser the new location but really it is just putting https in front 
    //of the current url
    header("Location: http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit(); //exit the excution of any other php code
}
$db = new DbObject();
if ($_SESSION['admin'] != 'Y')
{
    header("location: login.php?admin=false");
}
if (isset($_POST["approve"]) || isset($_POST["delete"]))
{
    if (isset($_POST["approve"]))
    {
        $stmt = $db->prepare("UPDATE cst214Images SET reported = 'A' 
            WHERE imageID = ?");
        $stmt->bind_param("s", $_SESSION["imageID"]);
        if ($stmt->execute())
        {
            $_SESSION["lastAction"] = 'Approval successful';
        }
        else
        {
            $_SESSION["lastAction"] = 'Approval not successful';
        }
    }
    else
    {

//             $selQuery = "Select imageID, metatype FROM cst214Images";
//             $stmt4 = $db->prepare( $selQuery );
//             $stmt4->bind_result($imageID, $metatype);
//             $stmt4->execute();   
//             $stmt4->fetch();
//             $stmt4->close();
//    
//            $metatype = substr($metatype, -3);
//            $name =$imageID.$metatype;
// 
//    
//            $filename = "../wide_open/$name";
//            unlink(realpath($filename));

        $query = "DELETE FROM cst214Images WHERE imageID=?";
        $stmt = $db->prepare($query);
        $stmt->bind_param("s", $_SESSION["imageID"]);
        if ($stmt->execute())
        {
            $_SESSION["lastAction"] = 'Deletion successful';
        }
        else
        {
            $_SESSION["lastAction"] = 'Deletion not successful';
        }
    }
    unset($_SESSION['imageID']);

    $stmt->close();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./CSS/Style.css">
        <title>Admin</title>
        <script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
        <script type="text/javascript">


            function populate(xml)
            {
                $(xml).find('picture').each(
                        function() {
                            var picture = $(this);
                            var row = $('#image');
                            var imageID = picture.find('imageID').text();
                            if (imageID !== "")
                            {
                                $.get('setImageID.php', {imageID: imageID});
                                var location = "../wide_open/" + picture.find('imageID').text() + "." + picture.find('metatype').text();
                                row.append("<img src='" + location + "'/>");
                            }
                            else
                            {
                                $('#pic').css("display", "none");
                            }
                            $('#lastAction').append(picture.find('lastAction'));
                            $('#caption').append(picture.find('caption'));

                        });
            }

            function init()
            {
                $.get('getPicture.php', populate);

            }

            $(init);
        </script>
    </head>
    <body>
        <div id="framecontentLeft">
            <div class="innertube">
            </div>
        </div>
        <div id="framecontentRight">
            <div class="innertube">
            </div>
        </div>
        <div id="maincontent">
            <div class="innertube">
                <h2 id="caption"></h2>
                <div id="image">

                </div>
<?php
$form = new GenerateForm();
$form->startForm("pic", "Reported Images");
?>
                <div>
                    <input type="submit" value="Approve" name="approve" />
                    <input type="submit" value="Delete" name="delete"/>
                </div>
                </fieldset>
                </form>
                <p id='lastAction'></p>

                <a href="index.php">Back</a>
            </div>
        </div>
    </body>
</html>
<?php

function __autoload($className)
{
    $fileName = "$className.class.php";
    if (file_exists("$fileName"))
    {
        //require_once("../classes/$fileName");
    }
    else
    {
        require_once("./classes/$fileName");
    }
}
?>

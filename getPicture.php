<?php   
    session_start();
    require_once './classes/XmlDbObject.class.php';
    header( "Content-type: text/xml" );
    $xdb = new XmlDbObject();
    $stmt1 = $xdb->prepare("SELECT imageID, metatype, caption FROM cst214Images WHERE reported = 'Y'");

    $stmt1->bind_result($imageID, $metatype, $caption);
    $stmt1->execute();
    if (isset($_SESSION['lastAction']))
    {
        $lastAction = $_SESSION['lastAction'];
    }
    else
    {
        $lastAction = "";
    }
    
    if ($stmt1->fetch())
    {
        $infoData[] = array("imageID"=>$imageID, "caption"=>$caption, "metatype"=>$metatype, "lastAction"=>$lastAction);
    }
    else
    {
        $infoData[] = array("imageID"=>"", "caption"=>"No images left to approve", "lastAction"=>$lastAction);
    }
    $stmt1->close();

    $rootNode = $xdb->convertToXml( null, $infoData, "pictures", "picture");

    // Output XML string - using asXML to convert to string
    echo $rootNode->asXML();
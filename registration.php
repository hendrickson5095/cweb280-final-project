<?php
session_start();
if (!isset($_SERVER["HTTPS"]) || !$_SERVER["HTTPS"])
{
    // just checked to see if the current session isusing the https protocol /ssl
    //now we can use php yo redirect the browser to use https
    header("HTP/1.1 301 Moved Permanently"); // tells the browser the page is not longer here
    //tells the browser the new location but really it is just putting https in front 
    //of the current url
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit(); //exit the excution of any other php code
}
$db = new DbObject();
if (isset($_SESSION["username"]))
{

    header("Location: personalPage.php");
}

$errorMessage = "";
if (isset($_POST["subRegister"]))
{
    if (!empty($_POST["username"]) && !empty($_POST["password"]) && !empty($_POST["email"]) && !empty($_POST["repassword"]))
    {
        $passwordCheck = new PasswordChecker();

        $result = $db->select("username", "cst214Users", "username = '{$_POST['username']}'");
        $row = $result->num_rows;
        if ($row < 1)
        {
            if (checkEmail($_POST['email']))
            {
                if ($_POST["password"] == $_POST["repassword"])
                {

                    $result = $passwordCheck->addUser($_POST["username"], $_POST["password"], $_POST["email"]);

                    if ($result == false)
                    {
                        $errorMessage = "Error occured";
                    }
                    else
                    {
                        $_SESSION["username"] = $_POST["username"];
                        $_SESSION["loggedIn"] = true;
                        header("Location: index.php");
                    }
                }
                else
                {
                    $errorMessage = "Passwords don't match";
                }
            }
            else
            {
                $errorMessage = "Email is not valid";
            }
        }
        else
        {
            $errorMessage = "Username is taken";
        }
    }
    else
    {
        $errorMessage = "Empty field";
    }
}

function checkEmail($emailRef)
{
    $result = false;
    if (preg_match("#^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$#i", $emailRef))
    {
        $result = true;
    }
    return $result;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./CSS/Style.css">
        <title>Log in to the CST Membership system</title>
        <style type="text/css">

            #error
            {
                color: red;
            }
        </style>
        <link rel="stylesheet" type="text/css" href="./CSS/Style.css">
    </head>
    <body>
        <div id="framecontentLeft">
            <div class="innertube">
            </div>
        </div>
        <div id="framecontentRight">
            <div class="innertube">
            </div>
        </div>
        <div id="maincontent">
            <div class="innertube">
                <?php
                $form = new GenerateForm();
                $form->startForm("login", "Register");
                $form->textbox("username", "Username");
                $form->textbox("email", "Email");
                $form->password("password", "Password");
                $form->password("repassword", "Retype Password");
                $form->endForm("subRegister", "Register");
                ?>
                <h2 id="error"><?php echo $errorMessage; ?></h2>
                <p>Already have an account? <a href="login.php">Click here</a> to login</p>  
            </div>
        </div>
    </body>
</html>
<?php

function __autoload($className)
{
    $fileName = "$className.class.php";
    if (file_exists("$fileName"))
    {
        //require_once("../classes/$fileName");
    }
    else
    {
        require_once("./classes/$fileName");
    }
}
?>
<?php
require_once './classes/XmlDbObject.class.php';

//fix the header for the file to ensure the browser knows it is an xml file
header("Content-type: text/xml");
$xdb = new XmlDbObject();

//METHOD 1: using prepared statement
//$stmt = $xdb->prepare("SELECT id,province FROM Provinces ORDER BY province");
$stmt = $xdb->prepare("SELECT imageID, metatype, caption FROM cst214Images ORDER BY views LIMIT 10");

//in this cae we are not using any passed in params do not need to bind_param

$stmt->bind_result($imageID,$metatype,$caption);
$stmt->execute();
 
//becasue stmt->get_result is not supported on our linux server
//we need to go through the extra step of putting our data into a 2D array
$infoData = array();
while($stmt->fetch()){
    
   $link= "<a href='image.php?img=$imageID' ' ><img src='../wide_open/$imageID.$metatype' height='128px' width='128px'></a>";
    // add values to 2D array
    //this will define the xml element names and their values
    $infoData[] = array("imageID"=>$imageID,"metatype"=>$metatype,"caption"=>$caption, "link"=>$link);
}
//we are done looping through the statement so close it
$stmt->close();

$rootNode = $xdb->convertToXml(null, $infoData, "images", "image");

//out put xml string - using asXML to convert to string
echo $rootNode->asXML();
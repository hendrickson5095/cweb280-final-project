<?php
session_start();
if (isset($_GET["admin"]))
{
    echo 'Must be logged in as an administrator to access';
}
if (!isset($_SERVER["HTTPS"]) || !$_SERVER["HTTPS"])
{
    // just checked to see if the current session isusing the https protocol /ssl
    //now we can use php yo redirect the browser to use https
    header("HTP/1.1 301 Moved Permanently"); // tells the browser the page is not longer here
    //tells the browser the new location but really it is just putting https in front 
    //of the current url
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit(); //exit the excution of any other php code
}
$errorMessage = "";
if (isset($_POST['login']))
{
    $db = new DbObject();
    $passwordCheck = new PasswordChecker();
    if ($passwordCheck->isValid($_POST["username"], $_POST["password"]))
    {
        $qryResults = $db->select("admin", "cst214Users", "username ='{$_POST['username']}'");
        $row = $qryResults->fetch_row();

        $_SESSION["username"] = $_POST["username"];
        $_SESSION['loggedIn'] = true;
        $_SESSION['admin'] = $row[0];
        header("Location: index.php");
    }
    else
    {
        $errorMessage = "Username/Password is invalid";
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link rel="stylesheet" type="text/css" href="./CSS/Style.css">

    </head>
    <body>
        <div id="framecontentLeft">
            <div class="innertube">
            </div>
        </div>
        <div id="framecontentRight">
            <div class="innertube">
            </div>
        </div>
        <div id="maincontent">
            <div class="innertube">
                <?php
                $form = new GenerateForm();
                $form->startForm("frmLogin", "Login");
                $form->textbox('username', 'Username');
                $form->password('password', 'Password');
                $form->endForm('login', 'Login');
                ?>
                <h2 id="error"><?php echo $errorMessage ?></h2>
                <p>Don't have an account? <a href="registration.php">Click here</a> to register</p>
                <p>Forgot your password? <a href="reset.php">Click here</a> to reset your password</p>
            </div>
        </div>
    </body>
</html>
<?php

function __autoload($className)
{
    $fileName = "$className.class.php";
    if (file_exists("$fileName"))
    {
        //require_once("../classes/$fileName");
    }
    else
    {
        require_once("./classes/$fileName");
    }
}
?>
